#!/usr/bin/env python3

"""
Converts MarkDown formatted comments into JSON which can be used as a
remote source. Shortens and corrects links along the way, as well.

Example input (line wrapped, note incorrect $SITEURL$ usage):

    ###[Q]Really a bug
    This question should instead be filed as a bug report, and
    [as such](http://meta.askubuntu.com/q/1317) is off-topic, thanks!
    [Instructions on filing a bug report are here](http://$SITEURL$/q/5121).

will produce, note the fixed $SITEURL$ link:

    callback(
      [
        {
          "name": "[Q]Really a bug",
          "description": "This question should instead be filed as a bug report, and [as such](http://meta.askubuntu.com/q/1317) is off-topic, thanks! [Instructions on filing a bug report are here](http://askubuntu.com/q/5121)."
        }
      ]
    )

"""

import sys
import json
import re

from collections import OrderedDict

md_string = sys.stdin.read()
comments = []

fix_sitename_sub = re.compile(
    r'http://\$SITEURL\$/(q|questions)/([0-9]+)'
)

shorten_url = re.compile(
    r'http://([a-z0-9.]+)/questions/([0-9]+)/?[0-9a-z-]*/?([0-9]*)#?\3'
)

for line in md_string.split('\n'):
    line = line.strip()
    if line:
        if line.startswith('###'):
            comments.append(OrderedDict(name=line.replace('###', ''),
                                        description=''))
        else:
            for match in fix_sitename_sub.finditer(line):
                line = line.replace(match.group(0),
                                    match.group(0).replace('$SITEURL$',
                                                           'askubuntu.com'))
            for match in shorten_url.finditer(line):
                if 0:
                    break
                post_id = match.group(3) or match.group(2)
                base_url = 'http://' + match.group(1)
                line = line.replace(match.group(0),
                                    base_url + '/q/' + post_id)
            comments[-1]['description'] += line

print('callback(')
print(
    '\n'.join(
        '  ' + line for line in json.dumps(
            comments,
            indent=2,
            separators=(',', ': ')
        ).split('\n')
    )
)
print(')')
